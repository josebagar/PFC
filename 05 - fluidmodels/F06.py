#!/usr/bin/env python3

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import gzip as gz
import numpy as np
import sys

class F06:
    """Only one subcase, SORT=2 and SYSTEM(9)=9999999 are supported, by now.
    If I ever implement readers for other Nastran output file formats, the F06 class will actually
    be a subclass of a common 'Results' class."""

    def __init__(self, path):
        """Create a F06 object for later use.
        This routine should support opening both uncompressed and gzip files.

        It assumes that you only have one subcase in your F06 file, which is kinda crappy..."""

        # First, try as a gzipped file, then as a normal one
        try:
            self.f = gz.open(path, 'rt')

            # Silly check to ensure opening was successful
            self.f.readline()
            self.f.seek(0, 0)
            self.type = 'gz'
        except OSError:
            try:
                self.f = open(path, 'r')
                self.type = 'file'
            except IOError:
                sys.stderr.write("Couldn't read {} as an F06 file, quitting\n".format(path))
                sys.exit(1)
        except FileNotFoundException:
            sys.stderr.write("Couldn't find file ({}), quitting\n".format(path))
            sys.exit(2)

        # Default values
        self.solseq = None
        self.oload = np.zeros((6,6))
        self.oload_totals = np.zeros(6)
        self.eig = {}
        self.cdisp = {}

        # Finally, store the path
        self.path = path

    def parse_executive(self):
        """Will read the Executive Control Section of the F06 file.

        Executive control section looks like this:
0        N A S T R A N    E X E C U T I V E    C O N T R O L    E C H O
0


     SOL 111
     CEND"""

        line = self.f.readline()
        while line:
            if line.strip().upper().startswith('CEND'):
                break

            # We store the solution sequence as a string since the user might be using
            # the non-numeric names of the sequences (SEMODES...)
            if line.strip().upper().startswith('SOL'):
                data = line.strip().split()
                if len(data) == 2:
                    self.solseq = data[1]

            line = self.f.readline()

    def parse_oload(self):
        """Will read the OLOAD entry from self.f and store it

        OLOAD section looks like this:

0                                                  OLOAD    RESULTANT
  SUBCASE/    LOAD
  DAREA ID    TYPE       T1            T2            T3            R1            R2            R3
0      100     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00
               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00
               FZ       ----          ----      -1.000000E+00 -1.300220E-01  2.000520E-01     ----
               MX       ----          ----          ----       0.000000E+00     ----          ----
               MY       ----          ----          ----          ----       0.000000E+00     ----
               MZ       ----          ----          ----          ----          ----       0.000000E+00
             TOTALS  0.000000E+00  0.000000E+00 -1.000000E+00 -1.300220E-01  2.000520E-01  0.000000E+00"""

        j = 0

        for i in range(3):
            line = self.f.readline()

        while line and not 'TOTALS' in line:
            data = line.split()
            for i in range(len(data)-6, len(data)):
                k = i-len(data)+6
                try:
                    val = float(data[i])
                except ValueError:
                    val = 0.
                self.oload[j,k] = val

            line = self.f.readline()
            j += 1

        if 'TOTALS' in line:
            data = line.split()
            for i in range(len(data)-6, len(data)):
                k = i-len(data)+6
                try:
                    val = float(data[i])
                except ValueError:
                    val = 0.
                self.oload_totals[k] = val

    def parse_eigv(self):
        """Will read the EIGENVALUES entry from self.f and store it

        EIGENVALUES section looks like this:

                                              R E A L   E I G E N V A L U E S
                                         (BEFORE AUGMENTATION OF RESIDUAL VECTORS)
   MODE    EXTRACTION      EIGENVALUE            RADIANS             CYCLES            GENERALIZED         GENERALIZED
    NO.       ORDER                                                                       MASS              STIFFNESS
        1         1        1.843360E+03        4.293437E+01        6.833218E+00        1.000000E+00        1.843360E+03
        2         2        1.601487E+04        1.265499E+02        2.014104E+01        1.000000E+00        1.601487E+04"""

        for i in range(4):
            line = self.f.readline()

        while True:
            try:
                n, e, eigv, rad, f, m, k = line.split()

                n = int(n)
                if not n in self.eig.keys():
                    eigv = float(eigv)
                    f = float(f)
                    m = float(m)
                    k = float(k)
                    self.eig[n] = {'eigv': eigv, 'f': f, 'm': m, 'k':k}

                line = self.f.readline()
            except ValueError:
                break

    def parse_cdisp(self, previous):
        """Will read the complex displacements entry from self.f and store it

        Complex displacement section looks like this:

      FREQUENCY =  3.000000E+00
                                       C O M P L E X   D I S P L A C E M E N T   V E C T O R
                                                          (REAL/IMAGINARY)

      POINT ID.   TYPE          T1             T2             T3             R1             R2             R3
0         1000      G     -7.873191E-19   5.355998E-18  -5.719353E-06   1.147586E-05  -2.563201E-04  -6.243272E-17
                           2.271202E-23  -5.524398E-23   9.687262E-12  -1.786267E-10   2.682378E-10   7.074190E-22"""

        if not previous.strip().startswith('FREQUENCY ='):
            return False

        freq = float( previous.split()[2] )

        for i in range(4):
            line = self.f.readline()

        # We'll read two lines to get the full complex displacement
        while True:
            try:
                e, g, t, x, y, z, rx, ry, rz = line.split()
                if e == '1':
                    break

                g = int(g)
                x = float(x)
                y = float(y)
                z = float(z)
                rx = float(rx)
                ry = float(ry)
                rz = float(rz)
                real = np.array( (x, y, z, rx, ry, rz) )

                line = self.f.readline()

                x, y, z, rx, ry, rz = line.split()
                x = float(x)
                y = float(y)
                z = float(z)
                rx = float(rx)
                ry = float(ry)
                rz = float(rz)
                imag = np.array( (x, y, z, rx, ry, rz) )

                cdisp = real + 1.j*imag

                # Store the data, but only if it's not already there
                try:
                    if g in self.cdisp[freq].keys():
                        sys.stderr.write("Refusing to add complex displacement ({}) for grid {}@{}".format(cdisp, g, freq))
                        continue

                except KeyError:
                    self.cdisp[freq] = {}

                self.cdisp[freq][g] = cdisp
                #sys.stdout.write("Adding cdisp {} for node {}@{}Hz\n".format(cdisp, g, freq))
                #sys.stdout.flush()

                line = self.f.readline()
            except ValueError:
                break

    def parse(self):
        """Generic F06 parsing utility"""
        oldline = ''
        line = self.f.readline()
        while line:
            if 'N A S T R A N    E X E C U T I V E    C O N T R O L    E C H O' in line:
                self.parse_executive()
            elif 'OLOAD    RESULTANT' in line:
                self.parse_oload()
            elif 'R E A L   E I G E N V A L U E S' in line:
                self.parse_eigv()
            elif 'C O M P L E X   D I S P L A C E M E N T   V E C T O R' in line:
                self.parse_cdisp(oldline)
            oldline = line
            line = self.f.readline()