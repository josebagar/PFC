#!/usr/bin/env python

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys

class node():
    def __init__(self, id=None, x=None, y=None, z=None, fluid=False):
        self.id = id
        self.x = x
        self.y = y
        self.z = z
        self.fluid = fluid

class elem():
    def __init__(self, id=None, g1=None, g2=None, g3=None, g4=None, kind=''):
        self.id = id
        self.g1 = g1
        self.g2 = g2
        self.g3 = g3
        self.g4 = g4
        self.kind = kind

class chacab():
    def __init__(self, id=None, g1=None, g2=None, g3=None, g4=None, g5=None,
                 g6=None, g7=None, g8=None, kind='CHACAB'):
        self.id = id
        self.g1 = g1
        self.g2 = g2
        self.g3 = g3
        self.g4 = g4
        self.g5 = g5
        self.g6 = g6
        self.g7 = g7
        self.g8 = g8
        self.kind = kind

    @property
    def defined(self):
        if (self.id and self.g1 and self.g2 and self.g3 and self.g4 and self.g5 and
            self.g6 and self.g7 and self.g8):
            return True

        return False


if __name__ == '__main__':
    # Read all the nodes and interesting elements in the Nastran input files
    nodes = {}
    els   = {}

    for file in ('absorber.bdf', 'dummy_structure.bdf'):
        with open(file, 'r') as a:
            line = a.readline()
            while line:
                line = line.strip()
                if line.startswith('GRID'):
                    id = int(line[8:16].strip())
                    x = float(line[24:32])
                    y = float(line[32:40])
                    z = float(line[40:48])
                    fluid = True if line[48:56].strip() == '-1' else False
                    nodes[id] = node(id, x, y, z, fluid)
                elif line.startswith('CACINF4'):
                    id = int(line[8:16].strip())
                    g1 = int(line[24:32])
                    g2 = int(line[32:40])
                    g3 = int(line[40:48])
                    g4 = int(line[48:56])
                    els[id] = elem(id, g1, g2, g3, g4, 'CACINF4')
                elif line.startswith('CQUAD4'):
                    id = int(line[8:16].strip())
                    g1 = int(line[24:32])
                    g2 = int(line[32:40])
                    g3 = int(line[40:48])
                    g4 = int(line[48:56])
                    els[id] = elem(id, g1, g2, g3, g4, 'CQUAD4')
                line = a.readline()

    cacinf = (i for i in els if els[i].kind == 'CACINF4')
    cquad = (i for i in els if els[i].kind == 'CQUAD4')

    # Construct the new CHACAB elements
    chacabs = {}
    delta = 1.E-4
    for eid in cacinf:
        # Create the six faces
        if (nodes[els[eid].g1].x == nodes[els[eid].g2].x == nodes[els[eid].g3].x ==
            nodes[els[eid].g4].x == 0.5):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].x == 0.501 and abs(nodes[nid].y - nodes[els[eid].g1].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g1].z) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].x == 0.501 and abs(nodes[nid].y - nodes[els[eid].g2].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g2].z) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].x == 0.501 and abs(nodes[nid].y - nodes[els[eid].g3].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g3].z) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].x == 0.501 and abs(nodes[nid].y - nodes[els[eid].g4].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g4].z) < delta):
                    chacabs[eid].g4 = nid
        elif (nodes[els[eid].g1].x == nodes[els[eid].g2].x == nodes[els[eid].g3].x ==
            nodes[els[eid].g4].x == -0.5):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].x == -0.501 and abs(nodes[nid].y - nodes[els[eid].g1].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g1].z) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].x == -0.501 and abs(nodes[nid].y - nodes[els[eid].g2].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g2].z) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].x == -0.501 and abs(nodes[nid].y - nodes[els[eid].g3].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g3].z) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].x == -0.501 and abs(nodes[nid].y - nodes[els[eid].g4].y) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g4].z) < delta):
                    chacabs[eid].g4 = nid
        elif (nodes[els[eid].g1].y == nodes[els[eid].g2].y == nodes[els[eid].g3].y ==
            nodes[els[eid].g4].y == 0.25):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].y == 0.251 and abs(nodes[nid].x - nodes[els[eid].g1].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g1].z) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].y == 0.251 and abs(nodes[nid].x - nodes[els[eid].g2].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g2].z) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].y == 0.251 and abs(nodes[nid].x - nodes[els[eid].g3].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g3].z) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].y == 0.251 and abs(nodes[nid].x - nodes[els[eid].g4].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g4].z) < delta):
                    chacabs[eid].g4 = nid
        elif (nodes[els[eid].g1].y == nodes[els[eid].g2].y == nodes[els[eid].g3].y ==
            nodes[els[eid].g4].y == -0.25):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].y == -0.251 and abs(nodes[nid].x - nodes[els[eid].g1].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g1].z) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].y == -0.251 and abs(nodes[nid].x - nodes[els[eid].g2].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g2].z) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].y == -0.251 and abs(nodes[nid].x - nodes[els[eid].g3].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g3].z) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].y == -0.251 and abs(nodes[nid].x - nodes[els[eid].g4].x) < delta and
                     abs(nodes[nid].z - nodes[els[eid].g4].z) < delta):
                    chacabs[eid].g4 = nid
        elif (nodes[els[eid].g1].z == nodes[els[eid].g2].z == nodes[els[eid].g3].z ==
            nodes[els[eid].g4].z == 0.2015):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].z == 0.2025 and abs(nodes[nid].y - nodes[els[eid].g1].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g1].x) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].z == 0.2025 and abs(nodes[nid].y - nodes[els[eid].g2].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g2].x) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].z == 0.2025 and abs(nodes[nid].y - nodes[els[eid].g3].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g3].x) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].z == 0.2025 and abs(nodes[nid].y - nodes[els[eid].g4].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g4].x) < delta):
                    chacabs[eid].g4 = nid
        elif (nodes[els[eid].g1].z == nodes[els[eid].g2].z == nodes[els[eid].g3].z ==
            nodes[els[eid].g4].z == -0.2015):
            # Create the CHACAB element with the
            chacabs[eid] = chacab(eid, g5=els[eid].g1, g6=els[eid].g2, g7=els[eid].g3,
                                  g8=els[eid].g4)
            for nid in nodes:
                if ( nodes[nid].z == -0.2025 and abs(nodes[nid].y - nodes[els[eid].g1].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g1].x) < delta):
                    chacabs[eid].g1 = nid
                elif ( nodes[nid].z == -0.2025 and abs(nodes[nid].y - nodes[els[eid].g2].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g2].x) < delta):
                    chacabs[eid].g2 = nid
                elif ( nodes[nid].z == -0.2025 and abs(nodes[nid].y - nodes[els[eid].g3].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g3].x) < delta):
                    chacabs[eid].g3 = nid
                elif ( nodes[nid].z == -0.2025 and abs(nodes[nid].y - nodes[els[eid].g4].y) < delta and
                     abs(nodes[nid].x - nodes[els[eid].g4].x) < delta):
                    chacabs[eid].g4 = nid

    # Output the grabbed data
    for eid in chacabs:
        if not chacabs[eid].defined:
            continue

        sys.stdout.write('CHACAB  {:8}       3{:8}{:8}{:8}{:8}{:8}{:8}\n'.format(
            chacabs[eid].id, chacabs[eid].g1, chacabs[eid].g2, chacabs[eid].g3,
            chacabs[eid].g4, chacabs[eid].g5, chacabs[eid].g6))
        sys.stdout.write('        {:8}{:8}\n'.format(chacabs[eid].g7, chacabs[eid].g8))
