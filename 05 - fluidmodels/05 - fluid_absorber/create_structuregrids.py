#!/usr/bin/env python

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os,sys

class grid:
    def __init__(self, id, x, y, z):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.id = int(id)

    def __repr__(self):
        return 'GRID*   {:>16}                {:>16}{:>16}\n{:>16}'.format(self.id, self.x, self.y, self.z)

class chacab:
    def __init__(self, id, g5, g6, g7, g8):
        self.id = int(id)
        self.g5 = int(g5)
        self.g6 = int(g6)
        self.g7 = int(g7)
        self.g8 = int(g8)

# Read the original fluid nodes
grids = {}
with open('fluid.bdf', 'r') as i:
    line = i.readline()
    while line:
        line = line.strip()
        if line.startswith('GRID'):
            grids[int(line[8:16])] = grid(line[8:16], line[24:32], line[32:40], line[40:48])
        line = i.readline()

# Now, read the CHACAB elements
chacabs = {}
with open('absorber.bdf', 'r') as i:
    line = i.readline()
    while line:
        line = line.strip()
        if line.startswith('CHACAB'):
            line2 = i.readline().strip()
            chacabs[int(line[8:16])] = chacab(line[8:16], line[56:64], line[64:72],
                line2[0:8], line2[8:16])
        line = i.readline()

used_grids = []

# Write the actual elements
for el in chacabs:
    for nodeid in (chacabs[el].g5, chacabs[el].g6, chacabs[el].g7, chacabs[el].g8):
        if not nodeid in used_grids:
            if nodeid > 2000000:
                sys.stdout.write('{}\n'.format(grid(nodeid, grids[nodeid-2000000].x,
                    grids[nodeid-2000000].y, grids[nodeid-2000000].z)))
            else:
                sys.stdout.write('{}\n'.format(grid(nodeid, grids[nodeid-1000000].x,
                    grids[nodeid-1000000].y, grids[nodeid-1000000].z)))
            used_grids.append(nodeid)
