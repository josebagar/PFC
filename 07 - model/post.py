#!/usr/bin/env python3

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import time

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import art3d
import matplotlib.pyplot as plt

from F06 import F06

class node():
    "Holds node information (id, x, y, z)"
    def __init__(self, id, x, y, z):
        self.id = id
        self.x  = float(x)
        self.y  = float(y)
        self.z  = float(z)

    def __repr__(self):
        return 'GRID    {:8}        {:8}{:8}{:8}      -1'.format(self.id, self.x, self.y, self.z)

def get_closest_node(x, y, z, nodes):
    "This function will get the node closest to given coordinates in nodes"

    # The minimum distance found, and it's node
    d2 = 1e6
    node = None

    for n in nodes:
        _d = (x-n.x)*(x-n.x) + (y-n.y)*(y-n.y) + (z-n.z)*(z-n.z)
        if _d < d2:
            d2 = _d
            node = n

    return node

# First of all, read the fluid nodes from the model
fluid_nodes = []
try:
    with open('fluid.bdf', 'r') as f:
        line = f.readline()
        while line:
            if line.startswith('GRID'):
                id = line[8:16]
                x  = line[24:32]
                y  = line[32:40]
                z  = line[40:48]
                fluid_nodes.append(node(id, x, y, z))
            line = f.readline()
except IOError:
    sys.stderr.write("Couldn't open fluid.bdf for reading, quitting\n")
    sys.exit(1)

# Read all the structural nodes
# First of all, read the fluid nodes from the model
structure_nodes = []
try:
    with open('bulk.bdf', 'r') as f:
        line = f.readline()
        while line:
            if line.startswith('GRID'):
                id = line[8:16]
                x  = line[24:32]
                y  = line[32:40]
                z  = line[40:48]
                structure_nodes.append(node(id, x, y, z))
            line = f.readline()
except IOError:
    sys.stderr.write("Couldn't open bulk.bdf for reading, quitting\n")
    sys.exit(1)

# Construct the ellipsoid
_u = np.linspace(0, 2 * np.pi, 7)   # u ϵ [0, 2π)
_v = np.linspace(0, np.pi, 5)       # v ϵ [0, π]
_u = np.delete(_u, -1)

ellipsoid_x = []
ellipsoid_y = []
ellipsoid_z = []
excited_nodes = []

print("Determining nodes whose response to plot")
t = time.time()
for u in _u:
    for v in _v:
        x =           1.8 * np.cos(u) * np.sin(v)		# 2.1
        y = 0.11554 + 0.5 * np.sin(u) * np.sin(v)       # 1.5
        z = 0.22003 + 1.8 * np.cos(v)                   # 2.1
        n = get_closest_node(x, y, z, fluid_nodes)
        if not n in excited_nodes:
        	excited_nodes.append(n)
print("Done! ({}s)".format(time.time()-t))
sys.stdout.flush()

# Plot the data, for debugging/visualization purposes
#art3d.zalpha = lambda *args:args[0]     # Disable distance-based alpha
#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#plt.hold(True)
#ax.scatter([n.x for n in excited_nodes], [n.y for n in excited_nodes],
#           [n.z for n in excited_nodes], color='r')
#ax.scatter([n.x for n in structure_nodes], [n.y for n in structure_nodes],
#           [n.z for n in structure_nodes], color='k', alpha=0.2)
#ax.plot([], [], color='b', label='Ellipsoid point') # I'll got to hell for this
#ax.plot([], [], color='r', label='Excited node') # But hey! It works!
#ax.plot([], [], color='k', label='Structural node')
#plt.suptitle('Ellipsoid vs. excited point', fontsize=20)
#ax.set_xlabel('x (m)')
#ax.set_ylabel('y (m)')
#ax.set_zlabel('z (m)')
#ax.legend(shadow=True)
#plt.show()

# Now, parse the model:
sys.stdout.write("Reading F06 file...\n")
t = time.time()
f06 = F06('ARA3_infinite_fluid_loaded.f06.gz')
f06.parse()
sys.stdout.write("F06 reading took {}s\n".format(time.time()-t))
sys.stdout.flush()

# First, extract the pressure response data
# Frequencies for which we have results
data = [ ]
freqs = list(f06.cpress.keys())
data.append( freqs )
h = 'Freq\t'
for node in excited_nodes:
	h += node.id+'\t'
	# The given complex pressure is not really in a+b*j form, but in modulus+angle*j
	Y = [f06.cpress[freq][int(node.id)][1] for freq in freqs]
	data.append(np.real(Y))

np.savetxt('pressures.csv', np.matrix(data).T, delimiter='\t', header=h)

# Now, extract the accelerometer data from the F06 file
accels = [ [ 1, -1.0200, 0.0812, -0.8970],  # bottom plate
           [ 2,  0.3405, 0.0812, -0.8970],
           [ 3, -0.7500, 0.0812, -0.7954],
           [ 4, -0.1873, 0.0812, -0.5922],
           [ 5, -0.8400, 0.0812, -0.3225],
           [ 6,  0.1873, 0.0812, -0.0995],
           [ 7, -0.7500, 0.0812,  0.0195],
           [ 8, -0.8400, 0.0812,  0.2875],
           [ 9,  0.7500, 0.0812,  0.4211],
           [10, -0.6310, 0.0812,  0.5584],
           [11,  0.6310, 0.0812,  0.6939],
           [12, -0.7500, 0.0812,  1.1675],

           [13,  1.0200, 0.1155, -0.7954],  # medium plate
           [14, -1.0200, 0.1155, -0.5922],
           [15,  0.1873, 0.1155, -0.4578],
           [16,  1.0200, 0.1155, -0.4578],
           [17,  1.0200, 0.1155, -0.0995],
           [18,  1.0200, 0.1155,  0.2875],
           [19, -0.0937, 0.1155,  0.4211],
           [20,  0.3405, 0.1155,  0.6939],
           [21, -0.2810, 0.1155,  0.8294],
           [22,  0.1873, 0.1155,  0.9645],
           [23,  0.0937, 0.1155,  1.2347],
           [24, -1.0200, 0.1155,  1.5390],
           [25, -0.6970, 0.1155,  1.5390],
           [26,  0.3405, 0.1155,  1.5390],
           [27,  0.6970, 0.1155,  1.5390],

           [28, -0.2810, 0.1499, -0.8970],      # top plate
           [29,  0.0000, 0.1499, -0.8970],
           [30,  0.7500, 0.1499, -0.8970],
           [31, -0.4800, 0.1499, -0.5250],
           [32,  0.7500, 0.1499, -0.5250],
           [33, -0.2810, 0.1499, -0.3225],
           [34, -0.5750, 0.1499, -0.2035],
           [35,  0.4960, 0.1499, -0.2035],
           [36, -1.0200, 0.1499, -0.0995],
           [37, -0.0937, 0.1499, -0.0995],
           [38, -0.3608, 0.1499,  0.0195],
           [39,  0.7500, 0.1499,  0.0195],
           [40,  0.0000, 0.1499,  0.6939],
           [41, -0.5750, 0.1499,  0.8294],
           [42, -1.0200, 0.1499,  1.2347],
           [43,  0.6970, 0.1499,  1.2347],
           [44,  1.0200, 0.1499,  1.2347],
           [45,  0.0000, 0.1499,  1.5390],
           [46,  1.0200, 0.1499,  1.5390],

           [47, -0.4952, 0.0812,  0.2875],  # bottom plate
           [48,  1.0200, 0.0812,  0.2875],
           [49, -0.3405, 0.0812,  0.5584],
           [50,  0.2810, 0.0812,  0.6939],
           [51, -0.4960, 0.0812,  0.8294],
           [52,  0.9300, 0.0812,  0.8294],
           [53,  0.5078, 0.0812,  0.9645],
           [54,  0.0000, 0.0812,  1.1675],
           [55,  0.3405, 0.0812,  1.5390] ]

# Get the displacement for all the given nodes, save them to another CSV file
data = []
data.append(freqs)
h = 'Freq\t'
for accel in accels:
	n = get_closest_node(accel[1], accel[2], accel[3], structure_nodes)
	h += '{} (Node {})\t'.format(accel[0], n.id.strip())
	data.append([f06.cdisp[freq][int(n.id)][1] for freq in freqs])

np.savetxt('displacements.csv', np.matrix(data).T, delimiter='\t', header=h)