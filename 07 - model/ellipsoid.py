#!/usr/bin/env python3

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import art3d
import matplotlib.pyplot as plt
import numpy as np
import sys

class node():
    "Holds node information (id, x, y, z)"
    def __init__(self, id, x, y, z):
        self.id = id
        self.x  = float(x)
        self.y  = float(y)
        self.z  = float(z)

    def __repr__(self):
        return 'GRID    {:8}        {:8}{:8}{:8}      -1'.format(self.id, self.x, self.y, self.z)

def get_closest_node(x, y, z):
    "This function will get the node closest to given coordinates in nodes"

    # The minimum distance found, and it's node
    d2 = 1e6
    node = None

    for n in fluid_nodes:
        _d = (x-n.x)*(x-n.x) + (y-n.y)*(y-n.y) + (z-n.z)*(z-n.z)
        if _d < d2:
            d2 = _d
            node = n

    return node

# First of all, read the fluid nodes from the model
fluid_nodes = []
try:
    with open('fluid.bdf', 'r') as f:
        line = f.readline()
        while line:
            if line.startswith('GRID'):
                id = line[8:16]
                x  = line[24:32]
                y  = line[32:40]
                z  = line[40:48]
                fluid_nodes.append(node(id, x, y, z))
            line = f.readline()
except IOError:
    sys.stderr.write("Couldn't open fluid.bdf for reading, quitting\n")
    sys.exit(1)

# Read all the structural nodes
# First of all, read the fluid nodes from the model
structure_nodes = []
try:
    with open('bulk.bdf', 'r') as f:
        line = f.readline()
        while line:
            if line.startswith('GRID'):
                id = line[8:16]
                x  = line[24:32]
                y  = line[32:40]
                z  = line[40:48]
                structure_nodes.append(node(id, x, y, z))
            line = f.readline()
except IOError:
    sys.stderr.write("Couldn't open bulk.bdf for reading, quitting\n")
    sys.exit(1)

# Construct the ellipsoid
_u = np.linspace(0, 2 * np.pi, 11)   # u ϵ [0, 2π)
_v = np.linspace(0, np.pi, 10)       # v ϵ [0, π]
_u = np.delete(_u, -1)

ellipsoid_x = []
ellipsoid_y = []
ellipsoid_z = []
excited_nodes = []

for u in _u:
    for v in _v:
        x =           2.1 * np.cos(u) * np.sin(v)
        y = 0.11554 + 1.5 * np.sin(u) * np.sin(v)
        z = 0.22003 + 2.1 * np.cos(v)
        n = get_closest_node(x, y, z)
        excited_nodes.append(n)
        ellipsoid_x.append(x)
        ellipsoid_y.append(y)
        ellipsoid_z.append(z)

# Plot the data, for debugging/visualization purposes
#art3d.zalpha = lambda *args:args[0]     # Disable distance-based alpha
#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#plt.hold(True)
#ax.scatter(ellipsoid_x, ellipsoid_y, ellipsoid_z, color='b')
#ax.scatter([n.x for n in excited_nodes], [n.y for n in excited_nodes],
#           [n.z for n in excited_nodes], color='r')
#ax.scatter([n.x for n in structure_nodes], [n.y for n in structure_nodes],
#           [n.z for n in structure_nodes], color='k', alpha=0.2)
#ax.plot([], [], color='b', label='Ellipsoid point') # I'll got to hell for this
#ax.plot([], [], color='r', label='Excited node') # But hey! It works!
#ax.plot([], [], color='k', label='Structural node')
#plt.suptitle('Ellipsoid vs. excited point', fontsize=20)
#ax.set_xlabel('x (m)')
#ax.set_ylabel('y (m)')
#ax.set_zlabel('z (m)')
#ax.legend(shadow=True)
#plt.show()

# Write the SLOAD cards to the output file
written = []
try:
    with open('ellipsoid_load.bdf', 'w') as o:
        for n in excited_nodes:
            if not n.id in written:
                o.write('SLOAD   1001    {:8}      1.\n'.format(n.id))
                written.append(n.id)
except IOError:
    sys.stderr.write('Crap! I was almost there :(\n')
    sys.exit(2)

sys.stdout.write('Done!\n')
sys.exit(0)