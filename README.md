# Introduction #

This repository contains the models and code used while developing my [PFC](PFC.pdf) (equivalent to a Master's Thesis) in aerospace engineering in the [Polythecnic University of Madrid](http://upm.es/). The work was directed by [Marcos Chimeno, PhD](http://chimeno.net/scientia/index.php?lang=en), with whom I share the copyright to this work.

Parts of the work have not been published because of industrial property/copyright reasons. For what is available here: the models available are published under a [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/) while code is available under the [Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Also, I developed the Nastran reading routines a little further for some other work after finishing the PFC. You can find the resulting code [here](https://gitlab.com/josebagar/nastranModelReader).

### Contents ###

The repository is organized as follows:

* `05 - fluidmodels`: contains the models used in Chapter 3 of the PFC. The chapter models the air surrounding an isotropic plate and its boundary conditions via different alternatives. It contains several subfolders:
    * `01 - base`: The base plate model. The plate is simply supported on its vertices. Used in Chapter 3.1 and as a reference/base for the rest of the models.
    * `02 - mfluid`: Models the air around the plate with Nastran's mfluid elements. Used in Chapter 3.2.
    * `03 - fluid`: Models the air around the plate with Nastran's standard fluid elements whose boundary condition is zero pressure gradient (a wall). Used in Chapter 3.3.
    * `04 - fluid_infinite`: Models the air around the plate with Nastran's standard fluid elements whose boundary condition is modeled with infinite elements (free radiation). Used in Chapter 3.4.
    * `05 - fluid_absorber`: Models the air around the plate with Nastran's standard fluid elements whose boundary condition is modeled with acoustic absorber elements to simulate free radiation. Used in Chapter 3.5.
* `06 - gapmodels`: contains the models used in Chapter 4 of the PFC. The chapter models an air gap inside two isotropic plates via different alternatives. It contains several subfolders:
    * `01 - base`: The base structural model of the two plates. The plates are simply supported on their vertices. No air gap is modeled but this model serves as a base for the rest of the model.
    * `02 - fluid`: A mesh of Nastran's standard fluid elements is modeled between the plates. The boundary condition for those fluid nodes not in contact with structural nodes is Nastran's default zero-gradient (wall). Used in Chapter 4.1.
    * `03 - fluid_infinite`: A model similar to the one in `02 - fluid` but the boundary condition of the fluid is changed to simulate free radiation with infinite acoustic elements. Used in Chapter 4.2.
    * `04 - fluid_absorber`: A model similar to the one in `02 - fluid` but the boundary condition of the fluid is changed to simulate free radiation with acoustic absorber elements. Used in Chapter 4.3.
* `07 - model`: This folder contains some scripts used to create the model/acoustic loads/postprocess the results in Chapter 5. The models themselves have not been included for industrial property reasons.
* `Meshing Material&Air.xlsx`: an spreadsheet with some mesh-size calculations for the FE models. This is required since the frequency range which can be studied with a model depends on its mesh size/structural properties.
* `doc/DOCUMENTATION.txt`: References used for developing the models/code.
* `PFC.pdf`: The thesis itself (in Spanish). It can be also fetched from [my personal homepage](https://josebagar.com/PFC/PFC.pdf) or from [UPM's Digital Archive](http://oa.upm.es/43984/).

### Required software ###

* The models were run with MSC Nastran 2013.1. They should work with more recent versions of Nastran, but use features not available in previous versions.
* The Python scripts were run with Python 3.3, although any later 3.x version should work.

### Further words ###
As I said earlier, the work in this repository was developed for my own thesis; I consider the models to be quite clean and they can be used as examples for other work but the code contains a lot of implicit assumptions that may or may not be true for others. The code also contains a lot of undocumented stuff and inefficiencies. Use it with caution.

Drop us a line to marcos.chimeno@upm.es and/or joseba.gar@gmail.com if you find this package useful!