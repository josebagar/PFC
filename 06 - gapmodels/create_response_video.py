#!/usr/bin/env python3

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
from F06 import F06
from model import model
from time import time

# The list of params for each fluid model
#params = {'modelfile': '01 - base/01_base_108.dat', 'f06file': '01 - base/01_base_108.f06.gz',
#          'videofile': '01 - base/01_base_108.mp4', 'title': 'No fluid'}
#params = {'modelfile': '01 - base/01_base_111.dat', 'f06file': '01 - base/01_base_111.f06.gz',
#          'videofile': '01 - base/01_base_111.mp4', 'title': 'No fluid (modal formulation)'}
#params = {'modelfile': '02 - fluid/02_fluid_108.dat', 'f06file': '02 - fluid/02_fluid_108.f06.gz',
#          'videofile': '02 - fluid/02_fluid_108.mp4', 'title': 'Fluid modelling'}
# DEBUG:
#params = {'modelfile': '02 - fluid/02_fluid.dat', 'f06file': '02 - fluid/02_fluid_lowfreq.f06',
#          'videofile': 'test.mp4', 'title': 'DEBUG'}
#params = {'modelfile': '02 - fluid/02_fluid_111.dat', 'f06file': '02 - fluid/02_fluid_111.f06.gz',
#          'videofile': '02 - fluid/02_fluid_111.mp4', 'title': 'Fluid modelling (modal formulation)'}
params = {'modelfile': '03 - fluid_infinite/03_fluid_infinite_108.dat',
          'f06file': '03 - fluid_infinite/03_fluid_infinite_108.f06.gz',
          'videofile': '03 - fluid_infinite/03_fluid_infinite_108.mp4',
          'title': 'Fluid + Infinite fluid boundary'}
#params = {'modelfile': '03 - fluid_infinite/04_fluid_infinite_111.dat',
#          'f06file': '03 - fluid_infinite/04_fluid_infinite_111.f06.gz',
#          'videofile': '03 - fluid_infinite/04_fluid_infinite_111.mp4',
#          'title': 'Fluid + Infinite fluid boundary (modal formulation)'}

excited_node = 1259
response_node = 3328

start_time = time()

# The model file and the results file must match...
m = model(params['modelfile'])
m.parse()
f06 = F06(params['f06file'])
f06.parse()

colorbar = None
frame = 0
progress = 0

def update_plot(freq):
    global colorbar
    global colormap
    global excited_node
    global frame
    global progress

    frame += 1

    # First, get the real part of the Z displacements for the excited node
    cdisp = np.array([f06.cdisp[freq][excited_node][2]])
    cdisp = np.real(cdisp)

    # Now, get the results for all the nodes on the plate
    C = np.array([f06.cdisp[freq][i][2] for i in f06.cdisp[freq].keys() if i >= 2326])
    C = np.real(C)/cdisp[0]

    # Update the data, matplotlib will do the rest
    plot.set_array(C)

    # Update the colorbar limits, also if the colorbar hasn't been created,
    # create it and set the colormap for the plot
    if colorbar == None:
        colorbar = fig.colorbar(plot, ax=plate)
        plot.set_cmap(colormap)

    # Ensure that 1.0 is always in the middle of the scale (must revise corner cases)
    if np.max(C) > np.abs(np.min(C)):
        if np.max(C) > 1.0:
            plot.set_clim(vmin=(2.-np.max(C)), vmax=np.max(C))
        else:
            plot.set_clim(vmin=np.min(C), vmax=(2.-np.min(C)))
    else:
        plot.set_clim(vmin=np.min(C), vmax=(2.-np.min(C)))

    colorbar.update_normal(plot)
    plate.set_title('$u_{{y,i}} / u_{{y,{}}} \\forall i$ (f={:.2f}Hz)'.format(excited_node, freq))

    # Print a progress bar, since the job can be long
    old_progress = progress
    progress = int(60*frame/len(f06.cdisp.keys()))
    if progress != old_progress:
        sys.stdout.write('\r|{}{}| ({}%)'.format('+'*progress, ' '*(60-progress),
                         int(100*frame/len(f06.cdisp.keys()))))
        sys.stdout.flush()
    return plot,

def init_plot():
    global excited_node
    global params

    plate.set_title('$u_{{y,i}} / u_{{y,{}}} \\forall i$'.format(excited_node))
    fig.suptitle('Out-of-plane displacement ({})'.format(params['title']), fontsize=20)
    sys.stdout.write("Video render progress:\n")
    return plot,

# Plot the data
colorbar = None
# List of colormaps at http://matplotlib.org/examples/color/colormaps_reference.html
colormap = cm.Spectral_r
#colormap = cm.YlOrBr
fig = plt.figure(figsize=(25, 9))

# The subplot holding the displacement for each frequency
plate = fig.add_subplot(121)
X = np.array([m.nodes[i].x for i in f06.cdisp[2.].keys() if i >= 2326])
Y = np.array([m.nodes[i].y for i in f06.cdisp[2.].keys() if i >= 2326])
plot = plate.scatter(X, Y, s=60, marker='o', linewidths=(0,))
plate.annotate('Studied node ({})'.format(response_node), xy=(m.nodes[response_node].x,
               m.nodes[response_node].y), xytext=(m.nodes[response_node].x+0.03,
               m.nodes[response_node].y+0.022),
               arrowprops=dict(facecolor='black', shrink=0.05))
plate.set_xlabel('x ($m$)')
plate.set_ylabel('y ($m$)')
plt.grid(b=True, which='major', linestyle='-')
plt.grid(b=True, which='minor')
plate.minorticks_on()

# The subplot holding the response for a single node
response = fig.add_subplot(122)
X = np.array([freq for freq in f06.cdisp.keys()])
Y = np.array([f06.cdisp[freq][response_node][2] for freq in X])
Y = np.real(Y)
Y0 = np.array([f06.cdisp[freq][excited_node][2] for freq in X])
Y0 = np.real(Y0)
Y = Y/Y0
response.semilogy(X, np.abs(Y), linestyle='solid')
response.grid(b=True, which='major', linestyle='-')
response.grid(b=True, which='minor')
response.minorticks_on()
response.set_title('$u_{{y,{}}} / u_{{y,{}}}$'.format(response_node, excited_node))
response.set_ylabel('Displacement (log)')
response.set_xlabel('Frequency ($Hz$)')

ani = animation.FuncAnimation(fig, update_plot, sorted(f06.cdisp.keys()), interval=25, blit=True,
                              init_func=init_plot)
ani.save(params['videofile'], fps=5, dpi=200)

sys.stdout.write('\nJob took {}s\n'.format(int(time()-start_time)))

sys.exit(0)

