#!/usr/bin/env python3

# Copyright 2014-2017 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import os
import sys

class point():
    """A point with a numerical ID and a position"""
    def __init__(self, id=-1, x=0., y=0., z=0.):
        self.id = int(id)
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.pos = np.array((self.x, self.y, self.z))

    def __repr__(self):
        return str(self.pos)

class model():
    """One day, Simba, everything the light touches will be yours."""

    def __init__(self, path):
        """Create a model object for later use."""

        try:
            self.f = open(path, 'r')
            self.f.close()
        except IOError:
            sys.stderr.write("Couldn't read file '{}', quitting\n".format(path))
            sys.exit(1)

        # Default values
        self.nodes = {}

        # Finally, store the path for later use
        self.path = path

    def getFields(self, line):
        blocks = []
        # Free field format
        if ',' in line:
            blocks = line.split(',')
            if len(blocks) > 10:
                blocks = blocks[0:10]

            return blocks, 'free'

        # Store the different cards
        blocks.insert(0, line[0:8].strip())
        field  = 1
        pos    = 8

        # Vars for short field notation
        width  = 8
        fields = 9

        # Vars for large field notation
        notation = 'short'
        if line[0:8].strip().endswith('*'):
            notation = 'large'
            width  = 16
            fields = 5

        while pos < 72:
            blocks.insert(field, line[pos:pos+width].strip())
            pos += width
            field += 1
        blocks.insert(field, line[pos:pos+8].strip())

        return (blocks, notation)

    def parse(self, path=None):
        "Read the BDF file, see what we understand"

        if not path:
            path = self.path

        olddir = os.getcwd()
        dir = os.path.dirname(path)
        if dir != '':
            os.chdir(dir)

        try:
            with open(os.path.basename(path), 'r') as f:
                line = f.readline()
                while line:
                    line = line.strip()

                    # Handle includes
                    if line.upper().startswith('INCLUDE'):
                        inc = line.replace('"', "'").split("'")[1]
                        self.parse(inc)

                    # Parse GRIDs
                    elif line.startswith('GRID'):
                        blocks, notation = self.getFields(line)
                        if notation == 'large':
                            sys.stderr.write("WARNING: GRID {} is written in large format, which isn't supported\n".format(blocks[1]))
                        else:
                            id = int(blocks[1])
                            if blocks[2] == '':
                                cid = 0
                            else:
                                cid = int(blocks[2])
                            x = float(blocks[3])
                            y = float(blocks[4])
                            z = float(blocks[5])
                            self.nodes[id] = point(id, x, y, z)
                    line = f.readline()

        except FileNotFoundError:
            sys.stderr.write("Cannot parse '{}' since I cannot find it\n".format(path))
            sys.exit(1)

        os.chdir(olddir)

